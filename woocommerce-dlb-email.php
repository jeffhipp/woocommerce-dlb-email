<?php
/**
 * Plugin Name: WooCommerce DLB Custom Email
 * Description: Creates a custom email as per Dallas Lighthouse for the Blind's requirements whenever an order is completed.
 * Author: Jeff Hipp
 * Version: 1.0-dev
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 *  Add the custom email to the list of emails WooCommerce loads
 *
 * @since 1.0
 * @param array $email_classes available email classes
 * @return array filtered available email classes
 */
function add_dlb_order_email($email_classes) {
  
  // include our custom email class
  require( 'includes/class-wc-dlb-email.php' );
  
  // add the email class to the list of email classes that WooCommerce loads
  $email_classes['WC_dlb_email'] = new WC_DLB_Email();
  
  return $email_classes;
  
}
add_filter( 'woocommerce_email_classes', 'add_dlb_order_email' );